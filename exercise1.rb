lines = []
File.open('configuration.txt').each do |line|
	lines.push(line.gsub(/\s+/,""))
end

lines.map!{|line| line.split('#').first}.select!{|line| line != ""}
lines.map!{|rule| rule.split("=")}

result = {}

lines.each do |l|
	first = l.first.to_sym
	second = l[1]
	is_integer = /^\d+$/.match(second)
	is_float = /^[-+]?[0-9]*\.?[0-9]+$/.match(second)
	is_boolean = /true|false|yes|off|no|on/.match(second)

	if is_integer
		result[first] = second.to_i
	elsif is_float
		result[first] = second.to_f
	elsif is_boolean
		result[first] = true if second =~ (/^(true|on|yes)$/i)
		result[first] = false if second =~ (/^(false|off|no)$/i)
	else
		result[l.first] = second
	end
end
p result