# Challenge # 1 - Regular Expressions 

This repository is the Challenge # 1 of an interview test for a Junior ROR developer position.
It uses regular expressions for parsing the configuration of a site into Hashes, to work in Ruby.

For testing it, you just have to type in the console ruby Exercise1.rb and it will give you the hashes of a default configuration file. This configuration
file is a .txt attached, if you want to try with another configuration file, just overwrite the actual configuration.txt file and run the script.

Luis Vega